package com.readlist.application.bo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;

import com.readlist.application.dao.ContentDao;

public class SaveContentTest {/*
	
	private JdbcTemplate jdbcTemplate;
	private ContentDao contentDao;
	
	@Before
	public void setup() throws Exception{
		
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"read-list-infra-context.xml","datasource.xml"});
		
		jdbcTemplate = (JdbcTemplate) context.getBean("jdbcTemplate");
		
		 ClassPathResource classPathResource = new ClassPathResource("sql/read-list-app-setup.sql"); 
		    BufferedReader bufferedReader = new BufferedReader(new FileReader(classPathResource.getFile()));
		    StringBuilder sql = new StringBuilder();
		    String line;
		    while((line = bufferedReader.readLine()) != null){
		    	sql.append(line.trim());
		    	if(line.contains(";")){
		    		jdbcTemplate.execute(sql.toString());	
		    		sql = new StringBuilder();
		    	}	    	
		    }
		    bufferedReader.close();
		    contentDao = (ContentDao) context.getBean("contentDao");  
		    
	}
	
	@Test
	public void testSaveContent() {
		contentDao.save("http://www.rediff.com","Link",
				null,
				new Date(),
				new Date(),
				"Rediff.com - India's leading portal which covers India news, Hindi Movies, Photos, Videos, Live Cricket Score, Stock updates from BSE and NSE, Bollywood news, Online shopping, Social networking, Rediffmail NG"
				,null);
		
		int rowCount = jdbcTemplate.queryForInt("select count(*) from Content");
		Assert.assertEquals(1, rowCount);
		
		
		Content content = contentDao.findById(1);
		
		Assert.assertEquals( "http://www.rediff.com", content.getLink().toString());
	    
	    
		
	}

*/}
