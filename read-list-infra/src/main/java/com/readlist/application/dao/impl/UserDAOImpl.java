package com.readlist.application.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.readlist.application.bo.User;
import com.readlist.application.dao.ContentDao;
import com.readlist.application.dao.UserDAO;

/**
 * Implements UserDAO using Hibernate and its Criteria API.
 */
@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    private ContentDao contentDao;

    public void save(User user) {
        contentDao.saveUser(user.getName(), user.getPassword(), user.getEmailId());
    }

    public User findByUsername(String userName) {
        User user = contentDao.getuser(userName, null);
        return user;
    }

}
