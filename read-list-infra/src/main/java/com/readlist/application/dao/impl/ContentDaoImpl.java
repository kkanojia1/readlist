package com.readlist.application.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.readlist.application.bo.Content;
import com.readlist.application.bo.Group;
import com.readlist.application.bo.User;
import com.readlist.application.dao.ContentDao;

public class ContentDaoImpl extends AbstractDaoImpl implements ContentDao {

	

	public Content findById(long id) {

		String sql = "select * from Content" + " where cid = ?";
		RowMapper<Content> mapper = new ContentMapper();
		return (Content) jdbcTemplate.queryForObject(sql, new Object[] { id },
				mapper);
	}

	public Set<Content> findContentByUser(long userId) {
		Set<Content> cls = new HashSet<Content>();
		String sql = "select content.cid as cid, content.cicon as cicon, content.cdesc, content.clink as clink, " +
		"content.created_date as created_date, content.accessed_date as accessed_date, " +
		"content.target_date as target_date, content.c_cache_data as c_cache_date, " +
		"group1.gid as GID, group1.gname as GNAME from content, group1, groupcontent where group1.pwid = ? " +
		"and groupcontent.gid = group1.gid and content.cid = groupcontent.cid"; 
		//String sql = "select * from Content, Group1 where ";
		//RowMapper<Content> mapper = new ContentMapper();
		cls = jdbcTemplate.query(sql, new Object[]{userId}, new ContentGroupExtractor());
		return cls;
	}

	public void save(String link, String type, String icon, Date creationDate,
			Date lastAccessDate, String cachedContent, List<Group> groups) {

		this.jdbcTemplate
				.update("insert into Content (cname,cicon, cdesc,clink,created_date, accessed_date,c_cache_data) values ( ?, ?, ?, ?, ?, ?, ?)",
						link, icon, type, link, creationDate, lastAccessDate,
						cachedContent);

	}

	private static class ContentMapper implements RowMapper<Content> {
		public Content mapRow(ResultSet rs, int rowNum) throws SQLException {
			Content content = null;
			
			content = new Content(rs.getLong("cid"), rs.getString("clink"), rs.getString("cdesc"),
					rs.getString("cicon"), rs.getDate("created_date"),
					rs.getDate("accessed_date"),
					rs.getString("c_cache_data"),rs.getDate("target_date"));				
			
			return content;
		}

	}
	
	public class ContentGroupExtractor implements ResultSetExtractor<Set<Content>> {

		  public Set<Content> extractData(ResultSet rs) throws DataAccessException, SQLException {			  
			  Map<Long, Content> contentMap = new HashMap<Long, Content>();
		    while (rs.next()) {    	  
		    	  Content content = new Content(rs.getLong("cid"), rs.getString("clink"), rs.getString("cdesc"),
							rs.getString("cicon"), rs.getDate("created_date"),
							rs.getDate("accessed_date"),
							rs.getString("c_cache_data"),rs.getDate("target_date"));
		    	  
		    	  if(contentMap.get(content.getId()) == null){
		    		  contentMap.put(content.getId(), content);
		    	  }		    	  
		    	  Content existingContent = contentMap.get(content.getId());		    	  
		    	  Group group = new Group();
					group.setId(rs.getInt("GID"));
					group.setName(rs.getString("GNAME"));				
					existingContent.addGroup(group);							    	  
		    }		    
		    return new HashSet<Content>(contentMap.values());
		  }

		}

	@Override
	public Set<Content> findContentByGroup(long groupId) {
		Set<Content> cls = new HashSet<Content>();
		String sql = "select content.cid as cid, content.cicon as cicon, content.cdesc, content.clink as clink, " +
		"content.created_date as created_date, content.accessed_date as accessed_date, " +
		"content.target_date as target_date, content.c_cache_data as c_cache_date, " +
		"group1.gid as GID, group1.gname as GNAME from content, group1, groupcontent where " +
		"groupcontent.gid = group1.gid and content.cid = groupcontent.cid and content.cid in (select cid from GroupContent where gid = ?)";
		cls = jdbcTemplate.query(sql, new Object[] { groupId }, new ContentGroupExtractor());
		return cls;
	}

	@Override
	public void delete(long contentId) {
		this.jdbcTemplate.update("delete from GroupContent where cid=?)", contentId);
		this.jdbcTemplate.update("delete from Content where cid=?)", contentId);
		return;

	}

	@Override
	public void setTargetDate(long contentId, Date targetDate) {
		this.jdbcTemplate
		.update("update Content SET target_date= ? where cid = ?",
				targetDate, contentId);
		
	}

	@Override
	public void removeContentFromGroup(long contentId, long groupId) {
		this.jdbcTemplate
		.update("delete from groupcontent where into cid = ? and gid = ?", contentId, groupId); 

		
	}
	
		public User getuser(String userName, String password) {
		return this.jdbcTemplate.query(
				"Select uid,ucode, uname, email from User where uname = ?",
				new Object[] { userName }, new UserExtractor());
	}

	public class UserExtractor implements ResultSetExtractor<User> {

		public User extractData(ResultSet rs) throws SQLException {
			if (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("UID"));
				user.setName(rs.getString("uname"));
				user.setEmailId(rs.getString("email"));
				user.setPassword(rs.getString("ucode"));
				return user;
			}
			return null;

		}
}

	public void saveUser(String username, String password, String email) {
		this.jdbcTemplate
				.update("insert into User (uid, ucode, uname,email,domain, dwid) values (1 , ?, ?, ?, ?, ?)",
						password, username, email, "", 0);

	}

}
