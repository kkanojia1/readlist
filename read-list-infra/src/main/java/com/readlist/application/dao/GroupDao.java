package com.readlist.application.dao;

import java.util.List;
import java.util.Set;

import com.readlist.application.bo.Group;

public interface GroupDao {

	public Set<Group> getGroupsByContent(long contentId);

	public List<Group> getGroupsByUser(int i);
	
	public Set<Group> getGroupsNotForContent(long contentId);

	public void addContentInGroups(long contentId, Set<Long> addList);

	public void removeContentFromGroups(long contentId,
			Set<Long> removeList);
	
	public void addGroup(String groupName, String groupDescription, String groupIcon, long walletId);
	
}
