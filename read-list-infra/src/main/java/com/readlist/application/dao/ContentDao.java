package com.readlist.application.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.readlist.application.bo.Content;
import com.readlist.application.bo.Group;
import com.readlist.application.bo.User;

public interface ContentDao {

	Set<Content> findContentByUser(long userId);

	void save(String link, String type, String icon, Date creationDate,
			Date lastAccessDate, String cachedContent, List<Group> groups);

	Content findById(long id);

	Set<Content> findContentByGroup(long groupId);

	void delete(long contentId);
	
	void setTargetDate(long contentId, Date targetDate);
	
	void removeContentFromGroup(long contentId, long groupId);
	
	void saveUser(String username, String password, String email);
	
	User getuser(String userName, String password);

}
