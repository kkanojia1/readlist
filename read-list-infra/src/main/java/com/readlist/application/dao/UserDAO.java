package com.readlist.application.dao;

import com.readlist.application.bo.User;


/**
 * Data access methods for working with local accounts.
 */
public interface UserDAO {

    void save(com.readlist.application.bo.User user);

    User findByUsername(String username);

}
