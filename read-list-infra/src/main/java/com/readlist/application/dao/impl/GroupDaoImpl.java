package com.readlist.application.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.readlist.application.bo.Group;
import com.readlist.application.dao.GroupDao;

public class GroupDaoImpl extends AbstractDaoImpl implements GroupDao {
	
	
	@Override
	public Set<Group> getGroupsByContent(long contentId) {
		Set<Group> gls = new HashSet<Group>();
		RowMapper<Group> mapper = new GroupMapper();
		gls.addAll(jdbcTemplate.query("Select GID, GNAME from GROUP1 where GID in (select GID from GROUPCONTENT WHERE CID = ?)", 
						new Object[] { contentId }, mapper));
		return gls;

	}

	@Override
	public List<Group> getGroupsByUser(int i) {
		List<Group> gls = new ArrayList<Group>();
		RowMapper<Group> mapper = new GroupMapper();
		gls.addAll(jdbcTemplate.query("Select GID, GNAME from GROUP1", 
						new Object[0] , mapper));
		return gls;
	}
	
	
	private static class GroupMapper implements RowMapper<Group> {
		public Group mapRow(ResultSet rs, int rowNum) throws SQLException {
			Group group = new Group();
			group.setId(rs.getInt("GID"));
			group.setName(rs.getString("GNAME"));
			return group;
		}

	}


	@Override
	public Set<Group> getGroupsNotForContent(long contentId) {
		Set<Group> gls = new HashSet<Group>();
		RowMapper<Group> mapper = new GroupMapper();
		//TODO add pwid clause
		gls.addAll(jdbcTemplate.query("SELECT GID, GNAME FROM GROUP1 WHERE GID NOT in (SELECT GID FROM GROUPCONTENT WHERE CID = ?)", 
						new Object[] { contentId }, mapper));
		
		return gls;
	}

	@Override
	public void addContentInGroups(long contentId, Set<Long> addList) {
		for(long id:addList)
			jdbcTemplate.update("insert into groupcontent (GID, CID, PREFID) values (?,?, 1)",new Object[] {id, contentId});
			   
	}

	@Override
	public void removeContentFromGroups(long contentId, Set<Long> removeList) {
		NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("removeList",removeList);
			parameters.addValue("contentId",contentId);
			namedParameterJdbcTemplate.update("delete from groupcontent where gid in (:removeList) and CID = :contentId",parameters);
			return;
		
	}
	
	public void addGroup(String groupName, String groupDescription, String groupIcon, long walletId){
		jdbcTemplate.update("insert into Group1 (GID, GNAME, GDESC, GICON, PWID) values ( ?, ?, ?, ?, ?, ?)",
				1, groupName, groupDescription, groupIcon, walletId);
	}
	
	

}
