package com.readlist.application.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;

public class AbstractDaoImpl {
	
	protected JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
