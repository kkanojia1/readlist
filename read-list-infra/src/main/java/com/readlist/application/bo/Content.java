package com.readlist.application.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Content {
	private long id;
	private String link;
	private String type;
	private String icon;
	private Date creationDate;
	private Date lastAccessDate;
	private Date targetDate;
	private String cachedContent;
	private List<Group> groups = new ArrayList<Group>();

	public Content(long id, String link, String type, String icon,
			Date creationDate, Date lastAccessDate, String cachedContent, Date targetDate) {
		super();
		this.id = id;
		this.link = link;
		this.type = type;
		this.icon = icon;
		this.creationDate = creationDate;
		this.lastAccessDate = lastAccessDate;
		this.cachedContent = cachedContent;
		this.targetDate = targetDate;		
	}
	
	

	public Date getTargetDate() {
		return targetDate;
	}
	
	
	public void addGroup(Group group){
		this.groups.add(group);
	}



	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}



	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public void setCachedContent(String cachedContent) {
		this.cachedContent = cachedContent;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastAccessDate() {
		return lastAccessDate;
	}

	public void setLastAccessDate(Date lastAccessDate) {
		this.lastAccessDate = lastAccessDate;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public String getCachedContent() {
		return cachedContent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((cachedContent == null) ? 0 : cachedContent.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((icon == null) ? 0 : icon.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result
				+ ((lastAccessDate == null) ? 0 : lastAccessDate.hashCode());
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		result = prime * result
				+ ((targetDate == null) ? 0 : targetDate.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Content other = (Content) obj;
		if (cachedContent == null) {
			if (other.cachedContent != null)
				return false;
		} else if (!cachedContent.equals(other.cachedContent))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (icon == null) {
			if (other.icon != null)
				return false;
		} else if (!icon.equals(other.icon))
			return false;
		if (id != other.id)
			return false;
		if (lastAccessDate == null) {
			if (other.lastAccessDate != null)
				return false;
		} else if (!lastAccessDate.equals(other.lastAccessDate))
			return false;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		if (targetDate == null) {
			if (other.targetDate != null)
				return false;
		} else if (!targetDate.equals(other.targetDate))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Content [id=" + id + ", link=" + link + ", type=" + type
				+ ", icon=" + icon + ", creationDate=" + creationDate
				+ ", lastAccessDate=" + lastAccessDate + ", targetDate="
				+ targetDate + ", cachedContent=" + cachedContent + ", groups="
				+ groups + "]";
	}

	

}
