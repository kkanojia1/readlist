package com.readlist.application.bo;

import java.util.Arrays;

public class Wallet {
	
	private long Id;
	private Byte[] Skin;
	private String name;
	private long preferenceId;
	private long defaultGroupId;
	
	
	
	public Wallet(long id, Byte[] skin, String name, long preferenceId,
			long defaultGroupId) {
		super();
		Id = id;
		Skin = skin;
		this.name = name;
		this.preferenceId = preferenceId;
		this.defaultGroupId = defaultGroupId;
	}
	
	
	
	public long getId() {
		return Id;
	}



	public void setId(long id) {
		Id = id;
	}



	public Byte[] getSkin() {
		return Skin;
	}



	public void setSkin(Byte[] skin) {
		Skin = skin;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public long getPreferenceId() {
		return preferenceId;
	}



	public void setPreferenceId(long preferenceId) {
		this.preferenceId = preferenceId;
	}



	public long getDefaultGroupId() {
		return defaultGroupId;
	}



	public void setDefaultGroupId(long defaultGroupId) {
		this.defaultGroupId = defaultGroupId;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (Id ^ (Id >>> 32));
		result = prime * result + Arrays.hashCode(Skin);
		result = prime * result
				+ (int) (defaultGroupId ^ (defaultGroupId >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (int) (preferenceId ^ (preferenceId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Wallet other = (Wallet) obj;
		if (Id != other.Id)
			return false;
		if (!Arrays.equals(Skin, other.Skin))
			return false;
		if (defaultGroupId != other.defaultGroupId)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (preferenceId != other.preferenceId)
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Wallet [Id=" + Id + ", Skin=" + Arrays.toString(Skin)
				+ ", name=" + name + ", preferenceId=" + preferenceId
				+ ", defaultGroupId=" + defaultGroupId + "]";
	}
	
	
	
}
