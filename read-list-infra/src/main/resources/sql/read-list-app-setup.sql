Create Table	User	(
uid	number(10)	not null AUTO_INCREMENT(2,1),
ucode	varchar2(50)	not null, 
uname	varchar2(100)	null, 
email	varchar2(100)	null, 
domain	varchar2(100)	null,
dwid	number(10)	null	/* Default Wallet Id */);							

Create Table	Wallet	(		
	wid	number(10)	not null AUTO_INCREMENT(1,1),	
	wname	varchar2(50)	not null,	
	wdesc	varchar2(100)	null,	
	wicon	varchar2(100)	null,	
	puid	number(10)	null,	/* Parent User Id */
	dgid	number(10)	null,	/* Default Group Id */
	prefid	number(10)	null	
	);
				
Create Table	Group1	(		
	gid	number(10)	not null AUTO_INCREMENT(1,1),	
	gname	varchar2(50)	not null,	
	gdesc	varchar2(100)	null,	
	gicon	varchar2(100)	null,	
	pwid	number(10)	null,
	prefid	number(10)	null	
	);			
				
Create Table	Content	(		
	cid	number(10)	not null AUTO_INCREMENT( 1, 1),		
	cname	varchar2(250)	not null,	
	cdesc	varchar2(250)	null,	
	clink	varchar2(250)	null,	
	cicon	varchar2(100)	null,	
	c_cache_data	varchar2(500)	null,	
	created_date	date	null,	
	accessed_date	date	null,	
	target_date  DATE null,
	groupcount	number(10)	null	
	);			
				
Create Table	GroupContent(			
	gid	number(10)	not null,	
	cid	number(10)	not null,	
	created	date	null,	
	accessed	date	null,	
	archived	date	null,	
	accesscount	number(10)	null,	
	prefid	number(10)	null	
	);			
				
Create Table	ContentPref	(		
	prefid	number(10)	not null,	
	ruletype	varchar(10)	not null,	/* 'A'ccess Date, 'C'reated Date, 'U'nlimited */
	measureunit	varchar(10)	null,	/* Y/M/D */
	measure	number(10)	null	
	);	
	

CREATE SEQUENCE	IF NOT EXISTS CONTENT_ID_SEQUENCE START WITH 1 INCREMENT BY 1;
	