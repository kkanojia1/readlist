
/*var req = new XMLHttpRequest();
req.open(
    "GET",
    "http://localhost:8080/read-list/response.xml",
    true);
req.onload = populateFrame;
req.send(null);
*/
$.ajax({
    type: "GET",
    url: 'http://localhost:8080/read-list/response.xml',    
    dataType: "xml",
    success: populateFrame
});


function populateFrame(xml) {
  var contents = $(xml).find("content");
  for (var i = 0, content; content = contents[i]; i++) {	  
    document.body.appendChild(constructContent(content));
  }
}

function constructContent(content) {
	
	
	/*
	 * <div>
		<table>
			<tr>
				<td rowspan="2"><img alt="No Image"
					src="http://in.lifestyle.yahoo.com/favicon.ico"></td>
				<td>If you call yourself a food lover, here is a checklist to
					see how well you score. Synonymous with the places they come from,
					these ten dishes are as high on history as on taste. Start sampling
					now!</td>
			</tr>
			<tr>
				<td><a
					href="http://in.lifestyle.yahoo.com/10-indian-dishes-must-try-233805577.html">http://in.lifestyle.yahoo.com/10-indian-dishes-must-try-233805577.html</a></td>
			</tr>
		</table>
	</div>
	 */
	
	var parentDiv = document.createElement("div");
	var table = document.createElement("table");
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	td.setAttribute("rowspan", 2);
	var image = document.createElement("img");
	image.setAttribute("alt", "No Image");
	image.setAttribute("src", content.getAttribute("image"));
	td.appendChild(image);
	tr.appendChild(td);
	var td1 = document.createElement("td");
	td1.textContent=content.getAttribute("cached");
	tr.appendChild(td1);
	table.appendChild(tr);
	
	var tr1 = document.createElement("tr");
	var td11 = document.createElement("td");
	
	var a = document.createElement("a");
	a.setAttribute("href", content.getAttribute("link"));
	a.textContent=content.getAttribute("link");
	td11.appendChild(a);
	tr1.appendChild(td11);
	table.appendChild(tr1);
	parentDiv.appendChild(table);
	
	
  return parentDiv;
}
