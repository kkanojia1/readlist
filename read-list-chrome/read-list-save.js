function addToReadListOnClick(info, tab) {
	
	console.log("item " + info.menuItemId + " was clicked");
	console.log("info: " + JSON.stringify(info));
	console.log("tab: " + JSON.stringify(tab));
	
	var linkUrl;
	
	linkUrl = info.srcUrl;
	if(linkUrl == null)
		linkUrl = info.frameUrl;
	if(linkUrl == null)
		linkUrl = info.linkUrl;
	if(linkUrl == null)
		linkUrl = info.pageUrl;
	
	var mediaType = info.mediaType;
	
	var iconUrl = tab.favIconUrl;
	var tabTitle  = tab.title;
	var selectionText  = info.selectionText; 
	console.log("info.srcUrl: " +info.srcUrl);
	
	console.log("info.frameUrl: " +info.frameUrl );
	
	console.log("info.pageUrl: " +info.pageUrl );
	
	console.log("info.linkUrl: " +info.linkUrl );
	
	console.log("info.selectionText: " +info.selectionText );
	
	console.log("info.mediaType: " +info.mediaType );
	
	console.log("tab.url: " +tab.url );
	
	console.log("tab.favIconUrl: " +tab.favIconUrl );
	
	console.log("tab.title: " +tab.title  );
	
	//var req = new XMLHttpRequest();
	//req.open("POST", "http://localhost:8080/read-list/SaveLinkServlet", true);

	//var params = "&url="+ linkUrl; 
	//+ "&url="+ linkUrl 
	//+ "&iconUrl="+ iconUrl 
	//+ "&mediaType="+ mediaType
	//+ "&selectionText="+ selectionText;
	
	
	
	  var method = 'POST';
	  var url = 'http://localhost:8080/read-list/SaveLinkServlet';
	  var params = {'title': tabTitle,
			  'mediaType': mediaType,
			  'selectionText': selectionText,
			  'linkUrl': linkUrl,
			  'iconUrl': iconUrl};

	  var xhr = new XMLHttpRequest();
	  xhr.open(method, url + '?' + stringify(params), true);
	  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	  xhr.onreadystatechange = function(data) {
	    //callback(xhr, data);
	  };
	
	  xhr.send();
	

	/*req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	req.send();
	
	req.onreadystatechange = function() {//Call a function when the state changes.
		if(req.readyState == 4 && req.status == 200) {
			//alert(req.responseText);
		}
	};*/
	
	return false;
}

function stringify(parameters) {
	  var params = [];
	  for(var p in parameters) {
	    params.push(encodeURIComponent(p) + '=' +
	                encodeURIComponent(parameters[p]));
	  }
	  return params.join('&');
	};


var addToReadListMenuItem = chrome.contextMenus.create({
	"title" : "Add to read-list",
	"contexts" :["all", "page", "frame", "selection", "link", "editable", "image", "video", "audio"],  
	"onclick" : addToReadListOnClick
});