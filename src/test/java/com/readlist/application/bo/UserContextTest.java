package com.readlist.application.bo;

import java.io.BufferedReader;
import java.io.FileReader;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;

import com.readlist.application.dao.ContentDao;

public class UserContextTest {
	
	private JdbcTemplate jdbcTemplate;
	private ContentDao contentDao;
	
	@Before
	public void setup() throws Exception{
		
		ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"WEB-INF/applicationContext.xml"});
		
		jdbcTemplate = (JdbcTemplate) context.getBean("jdbcTemplate");
		
		 ClassPathResource classPathResource = new ClassPathResource("sql/read-list-app-setup.sql"); 
		    BufferedReader bufferedReader = new BufferedReader(new FileReader(classPathResource.getFile()));
		    StringBuilder sql = new StringBuilder();
		    String line;
		    while((line = bufferedReader.readLine()) != null){
		    	sql.append(line.trim());
		    	if(line.contains(";")){
		    		jdbcTemplate.execute(sql.toString());	
		    		sql = new StringBuilder();
		    	}	    	
		    }
		    bufferedReader.close();
		    contentDao = (ContentDao) context.getBean("contentDao");  
		    
	}
	
	@Test
	public void testSaveUserContext() {
		
		contentDao.saveUser("Kunal", "Kunal", "Kunal@ReadLater.com");
		
		int rowCount = jdbcTemplate.queryForInt("select count(*) from User");
		Assert.assertEquals(1, rowCount);
	}
	
	@Test
	public void testgetUserContext() {
		
		contentDao.saveUser("Kunal", "Kunal", "Kunal@ReadLater.com");
		
		User user = contentDao.getuser("Kunal", "Kunal123");
		Assert.assertEquals(user.getName(), "Kunal");
		Assert.assertEquals(user.getEmailId(), "Kunal@ReadLater.com");
	}

}
