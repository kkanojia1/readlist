<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.readlist.application.dao.ContentDao"%>
<%@ page import="com.readlist.application.bo.Content"%>
<%@ page import="java.util.List"%>
<%@ page import="java.io.OutputStream"%>
<%@ page import="java.io.InputStream"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Read List Home Page</title>
</head>
<body>
	<table>
		<tr>
			<td align="left">
					<%
						List<Content> contentList = (List<Content>) request
								.getAttribute("CONTENT_LIST");
								int i = 1;
						for (Content content : contentList) {
					%>
					<div style="background-color:<% out.print(i%2==0?"#C1F0F6":"#C3E4ED");%>" >
						<table>
							<tr>
								<td rowspan="2"><img alt="No Image"
									src="<%=request.getContextPath()%>/myImageServlet" ></td>
								<td><%=content.getCachedContent()%></td>
							</tr>
							<tr>
								<td><a href="<%=content.getLink()%>"><%=content.getLink()%></a></td>
							</tr>
						</table>
					</div>
					<%
					i++;
						}
					%>
			</td>
		</tr>
	</table>
</body>
</html>