package com.readlist.application.dao.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.readlist.application.bo.Content;
import com.readlist.application.bo.Group;
import com.readlist.application.bo.User;
import com.readlist.application.dao.ContentDao;

public class ContentDaoImpl implements ContentDao {
	
	
	private JdbcTemplate jdbcTemplate;
	
	public Content findById(long id){
		
		 String sql = "select * from Content" + 
		            " where cid = ?";
		 
		 RowMapper<Content> mapper = new RowMapper<Content>() {
		        public Content mapRow(ResultSet rs, int rowNum) throws SQLException {
		        	Content content  = null;
					try {
						content = new Content(
						rs.getLong("cid"),
						new URI(rs.getString("clink")),
						rs.getString("cdesc"),
						null,
						rs.getDate("created_date"),
						rs.getDate("accessed_date"),
						rs.getString("c_cache_data"));
					} catch (URISyntaxException e) {
						e.printStackTrace();
					};
		            return content;
		        }
		    };
		    
		    return (Content) jdbcTemplate.queryForObject(sql, new Object[] {id}, mapper);
	}
	
	public List<Content> findContentByUser(long userId) {

		List<Content> cls = new ArrayList<Content>();
		try {
			
			cls.add(new Content(1,
					new URI("http://www.rediff.com"),
					"Link",
					null,
					new Date(),
					new Date(),
					"Rediff.com - India's leading portal which covers India news, Hindi Movies, Photos, Videos, Live Cricket Score, Stock updates from BSE and NSE, Bollywood news, Online shopping, Social networking, Rediffmail NG"));
			cls.add(new Content(2,
					new URI("http://www.xyz.com"),
					"Link",
					null,
					new Date(),
					new Date(),
					"xyz.com - India's leading portal which covers India news, Hindi Movies, Photos, Videos, Live Cricket Score, Stock updates from BSE and NSE, Bollywood news, Online shopping, Social networking, Rediffmail NG"));
			cls.add(new Content(3,
					new URI(
							"http://andykayley.blogspot.in/2008/06/how-to-inject-spring-beans-into.html"),
					"Link", null, new Date(), new Date(),
					"http://andykayley.blogspot.in/2008/06/how-to-inject-spring-beans-into.html"));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cls;
	}

	@Override
	public void save( String link, String type, String icon, Date creationDate,
			Date lastAccessDate, String cachedContent, List<Group> groups) {
		
		this.jdbcTemplate.update(
		        "insert into Content (cname,cicon, cdesc,clink,created_date, accessed_date,c_cache_data) values ( ?, ?, ?, ?, ?, ?, ?)", 
		        link, icon, type, link, creationDate, lastAccessDate, cachedContent);
		
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public User getuser(String userName, String password) {

		return this.jdbcTemplate.query("Select uid, uname, email from User where uname = ?", new Object[]{userName} , new UserExtractor());
	}
	
	
	public class UserExtractor implements ResultSetExtractor<User> {

		  @Override
		  public User extractData(ResultSet rs) throws SQLException {
		    User user = new User();rs.next();
		    user.setId(rs.getInt("UID"));
		    user.setName(rs.getString("uname"));
		    user.setEmailId(rs.getString("email"));
		    return user;
		  }

		}


	@Override
	public void saveUser(String username, String password, String email) {
		this.jdbcTemplate.update(
		        "insert into User (uid, ucode, uname,email,domain, dwid) values (1 , ?, ?, ?, ?, ?)", 
		        password, username, email, "", 0);
		
	}
	

}
