package com.readlist.application.dao;

import java.util.Date;
import java.util.List;

import com.readlist.application.bo.Content;
import com.readlist.application.bo.Group;
import com.readlist.application.bo.User;

public interface ContentDao {
	
	List<Content> findContentByUser(long userId);

	void save( String link, String type, String icon, Date creationDate, Date lastAccessDate, String cachedContent,
	 List<Group> groups);
	
	void saveUser(String username, String password, String email);
	
	User getuser(String userName, String password);
	
	Content findById(long id);

}
