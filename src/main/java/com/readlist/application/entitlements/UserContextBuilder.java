package com.readlist.application.entitlements;

import com.readlist.application.bo.User;
import com.readlist.application.dao.ContentDao;

public class UserContextBuilder {
	
	ContentDao contentDao;
	
	public ContentDao getContentDao() {
		return contentDao;
	}

	public void setContentDao(ContentDao contentDao) {
		this.contentDao = contentDao;
	}
	
	
	public User getUserContext(String userName, String password){
		
		return this.getUserFromDB(userName, password);
	}
	
	private User getUserFromDB(String userName, String password){
		
		return contentDao.getuser(userName, password);
		
	}
	

}
