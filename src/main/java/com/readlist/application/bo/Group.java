package com.readlist.application.bo;

import java.util.Arrays;
import java.util.List;

public class Group {
	
	private String name;
	private long preferenceId;
	private Byte[] icon;
	private String description;
	
	private List<Content> contentList;
	
	public Group(String name, long preferenceId, Byte[] icon, String description) {
		super();
		this.name = name;
		this.preferenceId = preferenceId;
		this.icon = icon;
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getPreferenceId() {
		return preferenceId;
	}
	public void setPreferenceId(long preferenceId) {
		this.preferenceId = preferenceId;
	}
	public Byte[] getIcon() {
		return icon;
	}
	public void setIcon(Byte[] icon) {
		this.icon = icon;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	public List<Content> getContentList() {
		return contentList;
	}
	public void setContentList(List<Content> contentList) {
		this.contentList = contentList;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + Arrays.hashCode(icon);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (int) (preferenceId ^ (preferenceId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (!Arrays.equals(icon, other.icon))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (preferenceId != other.preferenceId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Group [name=" + name + ", preferenceId=" + preferenceId
				+ ", icon=" + Arrays.toString(icon) + ", description="
				+ description + "]";
	}
	
	

}
