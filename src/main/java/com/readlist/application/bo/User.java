package com.readlist.application.bo;

import java.util.Date;

public class User {
	
	
	private long Id;
	private String Name;
	private String emailId;
	private long WalletId;
	private long preferenceId;
	private long userProfile;
	private Date lastAccessTime;
	
	public User(){
		super();
	}
	
	public User(long id, String name, String emailId, long walletId,
			long preferenceId, long userProfile, Date lastAccessTime) {
		super();
		Id = id;
		Name = name;
		this.emailId = emailId;
		WalletId = walletId;
		this.preferenceId = preferenceId;
		this.userProfile = userProfile;
		this.lastAccessTime = lastAccessTime;
	}
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		Id = id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public long getWalletId() {
		return WalletId;
	}
	public void setWalletId(long walletId) {
		WalletId = walletId;
	}
	public long getPreferenceId() {
		return preferenceId;
	}
	public void setPreferenceId(long preferenceId) {
		this.preferenceId = preferenceId;
	}
	public long getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(long userProfile) {
		this.userProfile = userProfile;
	}
	public Date getLastAccessTime() {
		return lastAccessTime;
	}
	public void setLastAccessTime(Date lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (Id ^ (Id >>> 32));
		result = prime * result + ((Name == null) ? 0 : Name.hashCode());
		result = prime * result + (int) (WalletId ^ (WalletId >>> 32));
		result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());
		result = prime * result
				+ ((lastAccessTime == null) ? 0 : lastAccessTime.hashCode());
		result = prime * result + (int) (preferenceId ^ (preferenceId >>> 32));
		result = prime * result + (int) (userProfile ^ (userProfile >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (Id != other.Id)
			return false;
		if (Name == null) {
			if (other.Name != null)
				return false;
		} else if (!Name.equals(other.Name))
			return false;
		if (WalletId != other.WalletId)
			return false;
		if (emailId == null) {
			if (other.emailId != null)
				return false;
		} else if (!emailId.equals(other.emailId))
			return false;
		if (lastAccessTime == null) {
			if (other.lastAccessTime != null)
				return false;
		} else if (!lastAccessTime.equals(other.lastAccessTime))
			return false;
		if (preferenceId != other.preferenceId)
			return false;
		if (userProfile != other.userProfile)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "User [Id=" + Id + ", Name=" + Name + ", emailId=" + emailId
				+ ", WalletId=" + WalletId + ", preferenceId=" + preferenceId
				+ ", userProfile=" + userProfile + ", lastAccessTime="
				+ lastAccessTime + "]";
	}
	
	

}
