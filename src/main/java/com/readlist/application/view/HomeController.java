package com.readlist.application.view;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.HttpRequestHandler;

import com.readlist.application.bo.Content;
import com.readlist.application.bo.User;
import com.readlist.application.dao.ContentDao;

public class HomeController implements HttpRequestHandler {
	
	ContentDao contentDao;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ContentDao getContentDao() {
		return contentDao;
	}

	public void setContentDao(ContentDao contentDao) {
		this.contentDao = contentDao;
	}

	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		User userContext = (User) session.getAttribute("UserContext");
		
		System.out.println("**User Context checked***");
		
		if(userContext == null){
			RequestDispatcher rd = request.getRequestDispatcher("/jsp/login.jsp");
			rd.forward(request, response);	
		}
		else{

			List<Content> contentList = contentDao.findContentByUser(1234);
			
			request.setAttribute("CONTENT_LIST", contentList);
			
			System.out.println("******* HELLO*******"+ userContext.getName());
		  
			
			 String destination = "/jsp/content.jsp";
			 
			RequestDispatcher rd = request.getRequestDispatcher(destination);
			rd.forward(request, response);	
		}
	   
		
		
		
	}
	
	
	
}
