package com.readlist.application.view;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestHandler;

public class ImageController  implements HttpRequestHandler {

	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		System.out.println(request.getContextPath());
		File file = new File("C:/Work/read-list-app/src/main/webapp/WEB-INF/favicon.ico");
		
		System.out.println("****** IMAGE FILE PATH ****** "+file.getAbsolutePath());
		
		new MimetypesFileTypeMap().getContentType(file);
		
		InputStream is = new BufferedInputStream(new FileInputStream(file));
		String mimeType = URLConnection.guessContentTypeFromStream(is);

	   
	    // Set content type
		response.setContentType(mimeType);

	    // Set content size
	 
		response.setContentLength((int)file.length());

	    // Open the file and output streams
	   
	    OutputStream out = response.getOutputStream();

	    // Copy the contents of the file to the output stream
	    byte[] buf = new byte[1024];
	    int count = 0;
	    while ((count = is.read(buf)) >= 0) {
	        out.write(buf, 0, count);
	    }
	    is.close();
	    out.close();
		
	}

}
