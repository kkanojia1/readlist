package com.readlist.application.view;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.HttpRequestHandler;

import com.readlist.application.bo.User;
import com.readlist.application.entitlements.UserContextBuilder;

public class LoginController implements HttpRequestHandler {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		System.out.println("*****IN login Servlet*******");
		
		String userName = request.getParameter("Username");
		String password = request.getParameter("Password");
		
		UserContextBuilder builder = new UserContextBuilder();
		User userContext = builder.getUserContext(userName, password);
		
		HttpSession session = request.getSession();
		session.setAttribute("UserContext", userContext);
		
	    String destination = "/HomeServlet";
		RequestDispatcher rd = request.getRequestDispatcher(destination);
		rd.forward(request, response);	
		
	}
	
}
