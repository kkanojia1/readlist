package com.readlist.application.service;

import com.readlist.application.bo.User;


public interface UserService {

    User findByLogin(String login);

    void registerUser(User user);
}
