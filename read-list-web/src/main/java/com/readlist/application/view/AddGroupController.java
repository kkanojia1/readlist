package com.readlist.application.view;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.HttpRequestHandler;

import com.readlist.application.bo.User;
import com.readlist.application.dao.GroupDao;

public class AddGroupController extends AbstractLoginCheckController  implements HttpRequestHandler {
	
	private GroupDao groupDao;
	
	public GroupDao getGroupDao() {
		return groupDao;
	}



	public void setGroupDao(GroupDao groupDao) {
		this.groupDao = groupDao;
	}



	@Override
	protected void doAction(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		User userContext = (User) session.getAttribute("UserContext");
		
		String groupName = request.getParameter(RequestParamterConstants.REQUEST_PARAMETER_GROUP_NAME);
		//String groupDescription = request.getParameter(RequestParamterConstants.REQUEST_PARAMETER_GROUP_DESCRIPTION);
		//String groupIcon = request.getParameter(RequestParamterConstants.REQUEST_PARAMETER_GROUP_ICON);
		
		groupDao.addGroup(groupName, null, null, userContext.getWalletId());		
		return;
	}

}
