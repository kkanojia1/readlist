package com.readlist.application.view;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestHandler;

import com.readlist.application.bo.User;
import com.readlist.application.dao.ContentDao;
import com.readlist.application.security.SecurityUtil;

public class SaveController  implements HttpRequestHandler {
	
	
	public final String MEDIA_TYPE = "mediaType";
	public final String TAB_TITLE = "tabTitle";
	
	public final String SELECTION_TEXT = "selectionText";
	public final String LINK_URL = "linkUrl";
	public final String ICON_URL = "iconUrl";
	
	
	
	ContentDao contentDao;
	

	public void handleRequest(HttpServletRequest request, HttpServletResponse arg1)
			throws ServletException, IOException {
		
		System.out.println("HEllo reached in saving");
		
		User user = SecurityUtil.getLoggedInUser();
		
		String iconUrl = request.getParameter(ICON_URL);
		String linkURI = request.getParameter(LINK_URL);
		String medaiType = request.getParameter(MEDIA_TYPE);
		String selectionText= request.getParameter(SELECTION_TEXT);
		String tabTitle = request.getParameter(TAB_TITLE);
		
		
		contentDao.save(linkURI, "undefined".equals(medaiType)?"Link":medaiType, iconUrl, new Date(), new Date(), "undefined".equals(selectionText)?tabTitle:selectionText, null);
		
		/*Set<String> keys = request.getParameterMap().keySet();
		
		for(String key: keys){
			System.out.println(key +"  "+request.getParameter(key));
			
		}*/
		
		/*contentDao.save(1, "http://www.rediff.com","Link",
				null,
				new Date(),
				new Date(),
				"Rediff.com - India's leading portal which covers India news, Hindi Movies, Photos, Videos, Live Cricket Score, Stock updates from BSE and NSE, Bollywood news, Online shopping, Social networking, Rediffmail NG"
				,null);*/
	}

	public ContentDao getContentDao() {
		return contentDao;
	}

	public void setContentDao(ContentDao contentDao) {
		this.contentDao = contentDao;
	}
	
}
