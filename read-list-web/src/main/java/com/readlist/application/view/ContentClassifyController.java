package com.readlist.application.view;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestHandler;

import com.readlist.application.bo.Group;
import com.readlist.application.dao.GroupDao;

public class ContentClassifyController extends AbstractLoginCheckController implements HttpRequestHandler{

	GroupDao groupDao;
	
	public GroupDao getGroupDao() {
		return groupDao;
	}

	public void setGroupDao(GroupDao groupDao) {
		this.groupDao = groupDao;
	}

	
	
	@Override
	protected void doAction(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		long contentId = Long.parseLong(request.getParameter(RequestParamterConstants.REQUEST_PARAMETER_CONTENT_ID));
		Set<Group> contentGroups = groupDao.getGroupsByContent(contentId);
		
		String[] groupList = request.getParameterValues(RequestParamterConstants.REQUEST_PARAMETER_GROUP);
		
		Set<Long> groupSet = new HashSet<Long>();
		
		for(int i =0; i<groupList.length; i++)
		{
			System.out.println(groupList[i]);
			groupSet.add(Long.parseLong(groupList[i]));
		}
		Set<Long> addList =  getNewGroups(contentGroups,groupSet);
		Set<Long> removeList = getRemovedGroups(contentGroups,groupSet);
		System.out.println("addList "+addList);
		System.out.println("removeList "+removeList);
		groupDao.addContentInGroups(contentId, addList);
		groupDao.removeContentFromGroups(contentId, removeList);
		
		return;
		
	}

	private Set<Long> getRemovedGroups(Set<Group> contentGroups, Set<Long> newGroupIdSet ) {
		Set<Long> existingContentGroupIdSet = new HashSet<Long>();
		for(Group contentGroup: contentGroups)
		{
			existingContentGroupIdSet.add(contentGroup.getId());
		}
		existingContentGroupIdSet.removeAll(newGroupIdSet);
		return existingContentGroupIdSet; 
	}

	private Set<Long> getNewGroups(Set<Group> contentGroups, Set<Long> newGroupIdSet ) {
		Set<Long> newContentGroupIdSet = new HashSet<Long>(newGroupIdSet);
		Set<Long> existingContentGroupIdSet = new HashSet<Long>();
		for(Group contentGroup: contentGroups)
		{
			existingContentGroupIdSet.add(contentGroup.getId());
		}
		newContentGroupIdSet.removeAll(existingContentGroupIdSet);
		return newContentGroupIdSet;
	}
	
	
}
