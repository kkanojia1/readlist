package com.readlist.application.view;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.readlist.application.bo.User;
import com.readlist.application.security.SecurityUtil;

/**
 * Controller for the home screen for logged in users.
 */
@Controller
public class LoginController {

    @RequestMapping("/")
    public ModelAndView home(HttpServletRequest request) {

        ModelAndView modelAndView = new ModelAndView("redirect:/HomeServlet");

        User user = SecurityUtil.getLoggedInUser();
        HttpSession session = request.getSession();
        session.setAttribute("user", user);

        modelAndView.addObject(user);

        return modelAndView;
    }
}
