package com.readlist.application.view;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestHandler;

import com.readlist.application.dao.ContentDao;

public class RemoveGroupFromContentContoller  extends AbstractLoginCheckController  implements HttpRequestHandler{

	private ContentDao contentDao;
	
	
	public ContentDao getContentDao() {
		return contentDao;
	}

	public void setContentDao(ContentDao contentDao) {
		this.contentDao = contentDao;
	}

	@Override
	protected void doAction(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		Long groupId = Long.parseLong(request.getParameter(RequestParamterConstants.REQUEST_PARAMETER_GROUP_ID));
		Long contentId = Long.parseLong(request.getParameter(RequestParamterConstants.REQUEST_PARAMETER_CONTENT_ID));
		
		contentDao.removeContentFromGroup(contentId, groupId);
		
	}
	
	

}
