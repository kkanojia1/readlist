package com.readlist.application.view;

public class ResponseAttributeConstants {
	
	public static final String CONTENT_LIST = "CONTENT_LIST";
	public static final String CONTENT = "CONTENT";
	public static final String GROUP_LIST = "GROUP_LIST";
	public static final String CONTENT_GROUP_LIST = "CONTENT_GROUP_LIST";

}
