package com.readlist.application.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;

import com.readlist.application.bo.User;
import com.readlist.application.security.SecurityUtil;
import com.readlist.application.service.UserService;

/**
 * Controller for handling the sign up (aka registration) process.
 *
 * Also handles when a new user signs in via Spring Social. The
 * Spring Social ProviderSignInController will redirect the new
 * user to the GET handler. In this app, a local account will be
 * automatically created for the new user and they will be signed
 * in to the Spring Security SecurityContext.
 */
@Controller
@RequestMapping("/signup")
public class SignUpController {

    private static final String FORM_VIEW = "signup";
    private static final String SUCCESS_VIEW = "redirect:/";

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(NativeWebRequest request, @ModelAttribute User user) {
        String view = FORM_VIEW;
        
        if(user.getName() !=null){

            userService.registerUser(user);

            view = SUCCESS_VIEW;
        }

        return view;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String registerUser(@ModelAttribute User user) {
        // user signing up via signup form

        // register user
        userService.registerUser(user);

        // sign user in
        SecurityUtil.signInUser(user);

        // send to user home page
        return SUCCESS_VIEW;
    }
}
