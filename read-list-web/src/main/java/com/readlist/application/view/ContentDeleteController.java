package com.readlist.application.view;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestHandler;

import com.readlist.application.dao.ContentDao;

public class ContentDeleteController extends AbstractLoginCheckController  implements HttpRequestHandler{
	
	ContentDao contentDao;

	public ContentDao getContentDao() {
		return contentDao;
	}

	public void setContentDao(ContentDao contentDao) {
		this.contentDao = contentDao;
	}

	
	@Override
	protected void doAction(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		long contentId = Long.parseLong(request.getParameter(RequestParamterConstants.REQUEST_PARAMETER_ID));
		
		contentDao.delete(contentId);
		
		String destination = "/jsp/content.jsp";

	
		RequestDispatcher rd = request.getRequestDispatcher(destination);
		rd.forward(request, response);
		
	}

}
