package com.readlist.application.view;

public class RequestParamterConstants {

	public static final String REQUEST_PARAMETER_ID = "id";
	public static final String REQUEST_PARAMETER_GROUP = "group";
	
	public static final String REQUEST_PARAMETER_CONTENT_ID = "contentid";
	
	public static final String REQUEST_PARAMETER_TARGET_DATE = "targetDate";
	
	public static final String REQUEST_PARAMETER_GROUP_NAME = "groupName";
	public static final String REQUEST_PARAMETER_GROUP_DESCRIPTION = "groupDescription";
	public static final String REQUEST_PARAMETER_GROUP_ICON = "groupIcon";
	public static final String REQUEST_PARAMETER_GROUP_ID = "ContentId";

}
