package com.readlist.application.view;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.readlist.application.bo.User;

public abstract class AbstractLoginCheckController {
	
	//TODO to find better method to do this.
	private boolean logincheck = false;
	
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User userContext = (User) session.getAttribute("user");
		
		System.out.println("**User Context checked***");
		String destination = null;
		if (userContext == null && logincheck) {
			destination = "/jsp/login.jsp";
			RequestDispatcher rd = request.getRequestDispatcher(destination);
			rd.forward(request, response);
		} else {
			doAction(request, response);
		}
	}

	protected abstract void doAction(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException;

}
