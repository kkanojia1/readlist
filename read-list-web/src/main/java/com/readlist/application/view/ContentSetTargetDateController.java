package com.readlist.application.view;

import java.io.IOException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestHandler;

import com.readlist.application.dao.ContentDao;

public class ContentSetTargetDateController extends
		AbstractLoginCheckController implements HttpRequestHandler {
	
	ContentDao contentDao;

	public ContentDao getContentDao() {
		return contentDao;
	}

	public void setContentDao(ContentDao contentDao) {
		this.contentDao = contentDao;
	}

	@Override
	protected void doAction(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		long contentId = Long.parseLong(request.getParameter(RequestParamterConstants.REQUEST_PARAMETER_CONTENT_ID));
		
		String targetDate = request.getParameter(RequestParamterConstants.REQUEST_PARAMETER_TARGET_DATE); 
		
		System.out.println("contentId "+contentId);
		System.out.println("targetDate "+targetDate);
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date date = sdf.parse(targetDate,new ParsePosition(0));
		contentDao.setTargetDate(contentId, date);
	}

}
