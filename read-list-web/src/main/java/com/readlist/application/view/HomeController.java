package com.readlist.application.view;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.HttpRequestHandler;

import com.readlist.application.bo.Content;
import com.readlist.application.bo.Group;
import com.readlist.application.bo.User;
import com.readlist.application.dao.ContentDao;
import com.readlist.application.dao.GroupDao;

public class HomeController extends AbstractLoginCheckController implements HttpRequestHandler {

	ContentDao contentDao;
	
	GroupDao groupDao;

	public ContentDao getContentDao() {
		return contentDao;
	}

	public void setContentDao(ContentDao contentDao) {
		this.contentDao = contentDao;
	}
		
	public GroupDao getGroupDao() {
		return groupDao;
	}

	public void setGroupDao(GroupDao groupDao) {
		this.groupDao = groupDao;
	}

	@Override
	protected void doAction(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String destination = null;

		Set<Content> contentList = contentDao.findContentByUser(1);
		request.setAttribute(ResponseAttributeConstants.CONTENT_LIST, contentList);
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		
		List<Group> groupList = groupDao.getGroupsByUser(1);
		request.setAttribute(ResponseAttributeConstants.GROUP_LIST, groupList);
		
		
		if ("chrome".equals(request.getParameter("window"))) {
			destination = "/jsp/chrome-content.jsp";
		} else {
			destination = "/jsp/content.jsp";

		}
		RequestDispatcher rd = request.getRequestDispatcher(destination);
		rd.forward(request, response);
		/*Gson gson = new Gson();
		String jsonString = gson.toJson(contentList);
		response.setContentType("text/x-json;charset=UTF-8");           
        response.setHeader("Cache-Control", "no-cache");        
        response.getWriter().write(jsonString);*/
	}

}
