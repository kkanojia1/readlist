package com.readlist.application.view;

import java.io.IOException;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.HttpRequestHandler;

import com.readlist.application.bo.Group;
import com.readlist.application.dao.GroupDao;

public class ContentGroupRetreiveController extends
		AbstractLoginCheckController implements HttpRequestHandler {
	
	GroupDao groupDao;
	
	
	
	public GroupDao getGroupDao() {
		return groupDao;
	}

	public void setGroupDao(GroupDao groupDao) {
		this.groupDao = groupDao;
	}

	@Override
	protected void doAction(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		long contentId = Long.parseLong(request.getParameter(RequestParamterConstants.REQUEST_PARAMETER_CONTENT_ID));
		
		//Set<Group> contentGroups = groupDao.getGroupsByContent(contentId);
		//request.setAttribute(ResponseAttributeConstants.CONTENT_GROUP_LIST, contentGroups);
		
		Set<Group> groups = groupDao.getGroupsNotForContent(contentId);
		request.setAttribute(ResponseAttributeConstants.GROUP_LIST, groups);
		
		String destination = "/jsp/content-group-xml.jsp";
	
		RequestDispatcher rd = request.getRequestDispatcher(destination);
		rd.forward(request, response);
		
	}

}
