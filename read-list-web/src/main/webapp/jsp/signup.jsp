<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>Read List</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Mere styles -->
    <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css" rel="stylesheet">
    <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet">

  </head>
  <body>	  
  <div class="container">
  <div class="row">
  <div class="span8">
	<div class="alert alert-success">
	  Complete this and get started. Its really simple.
	</div>

	<form:form id="registerHere" class="form-horizontal" name="regForm" commandName="user" method="post">

    <div id="global_errors">
        <form:errors path="*" cssClass="errors" element="div"/>
    </div>
    
	  <fieldset>
	    <legend>Registration</legend>
	    <div class="control-group">
	      <label class="control-label" for="input01">UserName</label>
	      <div class="controls">
	        <input type="text" class="input-xlarge" id="name" name="name" rel="popover" data-content="Enter a username of your choice." data-original-title="Name" >
	      </div>
	</div>
	
	 <div class="control-group">
		<label class="control-label" for="input01">Email</label>
	      <div class="controls">
	        <input type="text" class="input-xlarge" id="emailId" name="emailId" rel="popover" data-content="What's your email address?" data-original-title="Email">
	       
	      </div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="input01">Password</label>
	      <div class="controls">
	        <input type="password" class="input-xlarge" id="password" name="password" rel="popover" data-content="6 characters or more! Be tricky" data-original-title="Password" >
	       
	      </div>
	</div>
	<div class="control-group">
		<label class="control-label" for="input01"></label>
	      <div class="controls">
	       <button type="submit" class="btn btn-success" rel="tooltip" title="Create account">Create My Account</button>
	       
	      </div>
	</div>
	   
	  </fieldset>
	</form:form>
	</div>
		</div>
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>

    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://twitter.github.com/bootstrap/assets/js/jquery.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-transition.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-alert.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-modal.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-dropdown.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-scrollspy.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-tab.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-tooltip.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-popover.js"></script>
	<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
	  <script type="text/javascript">
	  $(document).ready(function(){
			$('#registerHere input').hover(function(){
					$(this).popover('show')
					},
					function(){
					$(this).popover('hide') 
					});
			$("#registerHere").validate({
				rules:{
					name:"required",
					emailId:{
							required:true,
							email: true
						},
					password:{
						required:true,
						minlength: 6
					}
				},
				messages:{
					name:"Pick a username",
					emailId:{
						required:"Enter your email address",
						email:"Enter valid email address"
					},
					password:{
						required:"Enter your password",
						minlength:"Password must be minimum 6 characters"
					}
				},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('success');
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
			});
		});
	  </script>

  </body>
</html>


