<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<title>Read List</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Mere styles -->
<link href="resources/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="resources/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="span8">
				<div class="alert alert-success">Complete this and store the link.</div>
				<form class="form-horizontal" id="group" method='post'
					action='newGroupForm.html'>
					<fieldset>
						<legend>Add Group</legend>
						<div class="control-group">
							<label class="control-label" for="input01">Group Name</label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="groupName" name="groupName" rel="popover" size = "35"
									data-content="What's Group Name."
									data-original-title="Group Name">
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<!--/.fluid-container-->

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="resources/javascript/jquery-1.8.2.min.js"
		type="text/javascript"></script>
	<script src="resources/javascript/jquery-ui-1.9.1.custom.min.js"
		type="text/javascript"></script>
	<script src="resources/javascript/bootstrap.js" type="text/javascript"></script>
	<script type="text/javascript"
		src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('#group input').hover(function() {
								$(this).popover('show')
							}, function() {
								$(this).popover('hide')
							});
							$("#group")
									.validate(
											{
												rules : {
													groupName : "required"
												},
												errorClass : "help-inline",
												errorElement : "span",
												highlight : function(element,
														errorClass, validClass) {
													$(element).parents(
															'.control-group')
															.removeClass(
																	'success');
													$(element).parents(
															'.control-group')
															.addClass('error');
												},
												unhighlight : function(element,
														errorClass, validClass) {
													$(element).parents(
															'.control-group')
															.removeClass(
																	'error');
													$(element)
															.parents(
																	'.control-group')
															.addClass('success');
												}
											});
						});
	</script>

</body>
</html>