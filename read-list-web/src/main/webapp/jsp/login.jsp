<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>MyLinkBucket</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css" rel="stylesheet">
    <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet">
  </head>
  <body>	  
  <div class="container">
  <div class="row">
  <div class="span8">

	<form class="form-horizontal" id="loginForm" name="loginForm" action="<c:url value="/j_spring_security_check"/>" method="post">
	 <c:if test="${not empty param.loginError}">
        <div id="errorsDiv">
            <div class="errors">
                Login and/or Password incorrect, please try again.<br>
            </div>
        </div>
    </c:if>
    
    <fieldset>
        <legend>Login</legend>
         <div class="control-group">
	      <label class="control-label" for="input01">Username</label>
		      <div class="controls">
		        <input type="text" class="input-xlarge" id="j_username" name="j_username" rel="popover" data-content="Yes your username goes here" data-original-title="UserName">
		      </div>
		</div>
        <div class="control-group">
		<label class="control-label" for="input01">Password</label>
	      <div class="controls">
	        <input type="password" class="input-xlarge" id="j_password" name="j_password" rel="popover" data-content="Yes that secret thing." data-original-title="Password" >
	      </div>
		</div>
		<div class="control-group">
		<label class="control-label" for="input01">Remember me</label>
	      <div class="controls">
	        <input type="checkbox" id="_spring_security_remember_me" name="_spring_security_remember_me"
                       value="true" checked="checked">
	      </div>
		</div>
		<div class="control-group">
			<label class="control-label" for="input01"></label>
		      <div class="controls">
		       <input type="submit" class="btn btn-success" id="submitBtn" name="submitBtn" class="submitBtn" value="Log In!">
		      </div>
		</div>
    </fieldset>
    
	</form>
	</div>
		</div>
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

    </div><!--/.fluid-container-->
    
     <div class="alert alert-success">
	 		<a href="<c:url value="/signup"/>">Dont have and account register now. Its simple.</a>
	</div>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://twitter.github.com/bootstrap/assets/js/jquery.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-transition.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-alert.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-modal.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-dropdown.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-scrollspy.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-tab.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-tooltip.js"></script>
    <script src="http://twitter.github.com/bootstrap/assets/js/bootstrap-popover.js"></script>
	<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
	  <script type="text/javascript">
	  $(document).ready(function(){
			$('#loginForm input').hover(function(){
					$(this).popover('show')
					},
					function(){
					$(this).popover('hide') 
					});
			$("#loginForm").validate({
				rules:{
					j_username:"required",
					j_password:{
						required:true,
					}
				},
				messages:{
					j_username:"How am i supposed to log you in.",
					j_password:{
						required:"Tell us that secret word now"
					}
				},
				errorClass: "help-inline",
				errorElement: "span",
				highlight:function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('success');
					$(element).parents('.control-group').addClass('error');
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).parents('.control-group').removeClass('error');
					$(element).parents('.control-group').addClass('success');
				}
			});
		});
	  </script>
	  
  </body>
</html>
