<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.readlist.application.dao.ContentDao"%>
<%@ page import="com.readlist.application.bo.Content"%>
<%@ page import="com.readlist.application.bo.Group"%>
<%@ page
	import="com.readlist.application.view.ResponseAttributeConstants"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.io.OutputStream"%>
<%@ page import="java.io.InputStream"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Link Bucket Home Page</title>
<link href="resources/css/bootstrap.css" rel="stylesheet" media="screen">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>
<link href="resources/css/bootstrap-responsive.css" rel="stylesheet"
	media="screen">
<script src="resources/javascript/jquery-1.8.2.min.js"
	type="text/javascript"></script>
<script src="resources/javascript/jquery-ui-1.9.1.custom.min.js"
	type="text/javascript"></script>
<script src="resources/javascript/bootstrap.js" type="text/javascript"></script>
<script>
function getGroups(contentid, event)
{
	 event.preventDefault();
	 $.ajax({
		    type: "GET",
		    url: '/read-list/ContentGroupRetreiveServlet',
		    data: { 
		        'contentid': contentid
		    },
		    dataType: "xml",
		    success: parseXml(contentid)
		});
};


function addLink(event)
{
	 event.preventDefault();
	 $.ajax({
		    type: "POST",
		    url: '/read-list/SaveLinkServlet',
		    data: $('#formContentLink').serialize() // serializes the form's elements.		    
		});
};


function addGroup(event)
{
	 event.preventDefault();
	 $.ajax({
		    type: "POST",
		    url: '/read-list/AddGroupServlet',
		    data: $('#form-new-group').serialize() // serializes the form's elements.		    
		});
	 cancelGroupForm();
};

function createDateForm(contentid, event){
	event.preventDefault();
	$('#con-tardate-'+contentid).append('<form id="form-con-tardate-'+contentid+'"> </form>');
	$('#form-con-tardate-'+contentid).append('<input type="hidden" name="contentid" value="'+contentid+'"/>');
	$('#form-con-tardate-'+contentid).append('Date: <input type="text" onclick="startDatePicker('+contentid+')" name="targetDate" id="datepicker-con-'+contentid+'" />');   
 	$('#form-con-tardate-'+contentid).append('<button class="btn btn-success" onclick="submitDateForm('+contentid+')">Ok</button>');
  	$('#form-con-tardate-'+contentid).append('<button class="btn" onclick="cancelDateForm('+contentid+')">Cancel</button>');
  	startDatePicker(contentid);
};

function createGroupForm(event){
	event.preventDefault();
	$('#add-group').append('<form id="form-new-group"></form>');
	$('#form-new-group').append('Group: <input type="text" name="groupName" id="groupName"/>');   
 	$('#form-new-group').append('<div id="group-button" class="row"></div>');
 	$('#group-button').append('<button onclick="submitGroupForm()" class="btn btn-success">Ok</button>');
  	$('#group-button').append('<button onclick="cancelGroupForm()" class="btn">Cancel</button>');
};

function removeGroupFromContent(contentId, groupId, event)
{
	event.preventDefault();
	$('#add-group').empty();	
	var url = "/read-list/AddGroupServlet"; // the script where you handle the form input.
  	$.ajax({
         type: "POST",
         url: url,
         data: $('#form-new-group'+contentid).serialize() // serializes the form's elements.
       });
  	
};

function cancelGroupForm(){
	$('#add-group').empty();
};

function submitGroupForm(){
	$('#add-group').empty();	
	var url = "/read-list/AddGroupServlet"; // the script where you handle the form input.
  	$.ajax({
         type: "POST",
         url: url,
         data: $('#form-new-group'+contentid).serialize() // serializes the form's elements.
       });
};

function submitDateForm(contentid){
 	var url = "/read-list/ContentSetTargetDateServlet"; // the script where you handle the form input.
  	$.ajax({
         type: "POST",
         url: url,
         data: $('#form-con-tardate-'+contentid).serialize() // serializes the form's elements.
       });
};

function startDatePicker(contentid){
	$( '#datepicker-con-'+contentid ).datepicker();
};

function cancelDateForm(contentid){
	$('#con-tardate-'+contentid).empty();
};
 
var parseXml = function(contentid) {
    return function(xml) {
    	//alert("am here");
    	var menu = $(xml).find("group");
    	var i = 0;
    	
    	if($('#form-con-group-'+contentid).length == 0){    	
	    	$('#con-group-'+contentid).append('<form id="form-con-group-'+contentid+'"> </form>');
	    	$('#form-con-group-'+contentid).append('<input type="hidden" name="contentid" value="'+contentid+'"/>');
	        menu.each(function() {
	        	i = 1;
	        	var id = $(this).attr('id');
	        	var name = $(this).attr('name');
	        	var selected = $(this).attr('selected');
	        	if(selected == 'true'){
	        		$('#form-con-group-'+contentid).append('<input type="checkbox" name="group" value="'+id+'" checked="'+selected+'">'+name+'<br>');
	        	}
	        	else{
	        		$('#form-con-group-'+contentid).append('<input type="checkbox" name="group" value="'+id+'">'+name+'<br>');
	        	}
	        });
	        if(i != 0 ){    	   	
	        	 $('#form-con-group-'+contentid).append('<div id="groups-button" class="row"></div>');	
	        	 $('#groups-button').append('<button onclick="submitGroupForm('+contentid+')" class="btn btn-success" value="Submit">Ok</button>');
		       	 $('#groups-button').append('<button onclick="cancelClassify('+contentid+')" style="margin-right: 5px" class="btn" value="Cancel">Cancel</button>');
	        }
    	}
    };
};

function cancelClassify(contentid){
	$('#con-group-'+contentid).empty();
};
   
function submitGroupForm(contentid){
 	var url = "/read-list/ContentClassifyServlet"; // the script where you handle the form input.
  	$.ajax({
         type: "POST",
         url: url,
         data: $('#form-con-group-'+contentid).serialize() // serializes the form's elements.
       });
};

</script>
</head>
<body>
	<div>
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container-fluid">
					<a class="btn btn-navbar" data-toggle="collapse"
						data-target=".nav-collapse"> <span class="icon-bar"></span> <span
						class="icon-bar"></span> <span class="icon-bar"></span>
					</a> <a class="brand" href="#">LinkBucket</a>
					<div class="nav-collapse collapse">
						<div class="row-fluid">
							<div class="span5">
								<ul class="nav">
									<li><a href="#about">About</a></li>
									<li><a href="#contact">Contact</a></li>
								</ul>
							</div>
							<div class="span5">
								<input type="text" id="Search" name="contentTitle" size="150">
								<a href=""><i class="icon-search"></i></a>
							</div>
						</div>
					</div>

					<!--/.nav-collapse -->
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span2">
					<div class="well sidebar-nav">
							<!-- Modal -->
							<a href="#myModal" data-toggle="modal" class="btn" ><i class=" icon-plus-sign" title="Add Link"></i></a>							
							<div id="myModal" class="modal hide fade" tabindex="-1"
									role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-hidden="true">×</button>
										<h4 id="myModalLabel">Add Link</h4>
									</div>
									<div class="modal-body">
										<form class="form-horizontal" id="formContentLink">
											<fieldset>
												<!-- <legend>Add Link</legend> -->
												<div class="control-group">
													<label class="control-label" for="input01">Title</label>
													<div class="controls">
														<input type="text" class="input-xlarge" id="title"
															name="contentTitle" rel="popover" size="150"
															data-content="What's Content Title."
															data-original-title="Content Title">
													</div>
												</div>

												<div class="control-group">
													<label class="control-label" for="input01">Link</label>
													<div class="controls">
														<input type="text" class="input-xlarge" id="linkUrl"
															name="linkUrl" rel="popover" size="150"
															data-content="What's content link?"
															data-original-title="Content Link">
													</div>
												</div>
											</fieldset>
										</form>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
										<button class="btn btn-primary">Save changes</button>
									</div>
								</div>
							
							<ul class="nav nav-list">																												
							<li class="nav-header">Links</li>
							<li class="active"><a href="#">Linkbox</a></li>
							<li class="nav-header">Groups</li>
							<%
								List<Group> allGroupList = (List<Group>) request
										.getAttribute(ResponseAttributeConstants.GROUP_LIST);
								for (Group group : allGroupList) {
							%>

							<li><a href="#"><%=group.getName()%></a></li>

							<%
								}
							%>
						</ul>
					</div>
					<!--/.well -->
				</div>
				<!--/span-->

				<div class="span10">
					<div class="row-fluid">
						<div class="span9">
							<div class="span2">
								<div class="btn-group">
									<button class="btn" onclick="createGroupForm(event)" title="Add Group">
										<i class="icon-folder-close"></i>
									</button>
									<span id="add-group" /></span>	
									<button class="btn" title="Mark Read">
										<i class="icon-ok"></i>
									</button>
									<button class="btn">
										<i class=" icon-trash" title="delete-link"></i>
									</button>	
									<button class="btn">
										<i class="icon-calendar"></i>
									</button>									
								</div>								
							</div>
						</div>
					</div>
					<%
						Set<Content> contentList = (Set<Content>) request
								.getAttribute("CONTENT_LIST");
						int i = 1;
						for (Content content : contentList) {
					%>
					<div class="row-fluid">
						<div class="span12">
							<div class="span1">
								<img alt="No Image" src="<%=content.getIcon()%>">
							</div>
							<div class="span11">
								<div class="row-fluid" style="height: 15px">
									<div class="span10" style="height: 15px; min-height: 15px" align="left"><b><%=content.getCachedContent()%></b></div>
									<div class="span2" style="height: 15px;  min-height: 15px">
										<a
											href="/read-list/ContentDeleteServlet?id=<%=content.getId()%>"><i
											class="icon-ok"></i></a>
										<!--a href="/read-list/ContentDeleteServlet?id=<!%=content.getId()%>">Delete</a-->
										<a onclick="getGroups(<%=content.getId()%>, event)" href=""><i
											class="icon-tags"></i></a>
										
										<a onclick="createDateForm(<%=content.getId()%>, event)"
											href=""> <%
 											out.print(content.getTargetDate() == null ? "<i class=\"icon-calendar\"></i>"
 											: content.getTargetDate()); %>
										</a>
									</div>
								</div>
								<div class="row-fluid" style="height: 15px;  min-height: 15px">
									<div class="span10" align="left" style="height: 15px;  min-height: 15px">
										<a href="<%=content.getLink()%>"><%=content.getLink()%></a>
									</div>
									<div class="span2" style="height: 15px;  min-height: 15px">
										<div id="con-group-<%=content.getId()%>"></div>
										<div id="con-tardate-<%=content.getId()%>" /></div>
									</div>
								</div>
								<div class="row-fluid">
									<%
										List<Group> groups = content.getGroups();
											for (Group group : groups) {
									%>
									<span
										style="background-color: rgb(245, 245, 245); border: 1px solid rgb(227, 227, 227); border-radius: 3px 3px 3px 3px;"><span
										style="padding-right: 5px;"><%=group.getName()%></span><a
										onclick="removeGroupFromContent(<%=content.getId()%>, <%=group.getId()%>, event)"
										href=""><sup>x</sup></a></span>
									<%
										}
									%>
								</div>
								<!-- </div> -->
							</div>
						</div>
					</div>
					<hr />
					<%
						i++;
						}
					%>
				</div>
			</div>
		</div>
	</div>
</body>
</html>