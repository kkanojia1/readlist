<%@ page language="java" contentType="application/xml"%>
<%@ page import="com.readlist.application.dao.ContentDao"%>
<%@ page import="com.readlist.application.bo.Group"%>
<%@ page import="com.readlist.application.view.ResponseAttributeConstants"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.io.OutputStream"%>
<%@ page import="java.io.InputStream"%>
<groups>
 	<%
	Set<Group> groupList = (Set<Group>) request
			.getAttribute(ResponseAttributeConstants.GROUP_LIST);
 	
 	Set<Group> contentGroupList = (Set<Group>) request
			.getAttribute(ResponseAttributeConstants.CONTENT_GROUP_LIST);
			
	for (Group group : groupList) {
		%> 
	<group name="<%=group.getName()%>" id="<%=group.getId()%>" selected="false"/>

	<%
	}
	%> 
</groups>