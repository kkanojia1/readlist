<%@ page language="java" contentType="application/xml"%>
<%@ page import="com.readlist.application.dao.ContentDao"%>
<%@ page import="com.readlist.application.bo.Content"%>
<%@ page import="java.util.List"%>
<%@ page import="java.io.OutputStream"%>
<%@ page import="java.io.InputStream"%>
<contents>
 	<%
	List<Content> contentList = (List<Content>) request
			.getAttribute("CONTENT_LIST");
			
	for (Content content : contentList) {
		%> 
	<content image="<%=content.getIcon()%>" link="<%=content.getLink()%>" cached="<%=content.getCachedContent()%>" />

	<%
	}
	%> 
</contents>